﻿using System;
using System.Xml.Serialization;

namespace practica_1_c
{
    class Program
    {
        static void Main(string[] args)
        {
            ejercicio4();
        }


        public static void ejercicio1()
        {
            //1.Programa que lea 10 calificaciones y calcule e imprima la cantidad de aprobados(notamayor o igual que 5)y de sobresalientes(notamayoro igual que 9). (2puntos)

            int numaprovados = 0;
            int numsobresalientes = 0;
            double nota;
            Console.WriteLine("Escriba 10 números");
            for (int i = 0; i < 10; i++)
            {
                nota = Double.Parse(Console.ReadLine());
                if (nota>=9)
                {
                    numsobresalientes++;
                }
                else if (nota>=5)
                {
                    numaprovados++;
                }
            }

            Console.WriteLine("Hay " + numaprovados + " aprovados");
            Console.WriteLine("Hay " + numsobresalientes + " sobresalientes");
        }
        public static void ejercicio2()
        {
            /* 2.La empresa X paga las facturas tres meses después de la fecha de recepción.
              Realice un programa que lea una fecha de recepción de factura e indique al
              usuario la fecha de pago.Suponga que todos los meses tienen 30 días.*/
            
            Console.WriteLine("Introduzca la fecha de recepción");
            DateTime fecharecep = DateTime.Parse(Console.ReadLine());
            fecharecep = fecharecep.AddMonths(3);
            DateTime  fechadev = fecharecep;
            Console.WriteLine("la fecha de pago es :");
            Console.WriteLine(fechadev);
            
            
        
        }

        public static void ejercicio3()
        {
            /* 3.Muestra el contenido del array numeros[10] desde el primer elemento al
               último(Debes generar primero el array y rellenarlo y luego recorrerlo).Añada
               el código necesario para que lo muestre también desde el último al primero, en
               esta ocasión con un bucle while.*/

            int[] array = new int[10];
            array[0] = 0;
            array[1] = 1;
            array[2] = 2;
            array[3] = 3;
            array[4] = 4;
            array[5] = 5;
            array[6] = 6;
            array[7] = 7;
            array[8] = 8;
            array[9] = 9;

            int contador;
            contador = 9;
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(array[i]);
            }
            while (contador > 0)
            {
                Console.WriteLine(array[contador]);
                contador--;
            }
        }

        public static void ejercicio4()
        {
            /*4.Programa que lea los siguientes datos acerca de 10 empresas: ingresos del mes y gastos del mes.
             * El programa debe calcular cuál de ellas (identificables como Empresa1, Empresa2, ..., Empresa10)ha obtenido el peor resultado (ingresos–gastos)eindicar dicho resultado. */
            int[] ingresos = new int[10];
            int[] gastos = new int[10];
            int resultado;
            int resultadofinal;
            int empresa;
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Datos Empresa" + (i + 1));
                ingresos[i] = Int32.Parse(Console.ReadLine());
                gastos[i] = Int32.Parse(Console.ReadLine());

            }
            resultadofinal = ingresos[0] - gastos[0];
                empresa = 1;
            for (int i = 0; i < 10; i++)
            {
                resultado = ingresos[i] - gastos[i];
                if (resultado<resultadofinal)
                {
                    resultadofinal = resultado;
                    empresa = (i + 1);
                }
            }
            Console.WriteLine("La empresa que obtuvo el peor resultado fue " + empresa + " con el resultado: " + resultadofinal);
        }

        public static void ejercicio5()
        {
            /*5.Programa que lea 10 valores numéricos y obtenga eimprima los dos mayores. Puede utilizar o no arrays. Si los usa debe generar una solución sin ordenar el array. */
            int[] array = new int[10];
            int mayor = 0;
            int mayor2 = 0;
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("Escribe número " + (i+1));
                array[i] = Int32.Parse(Console.ReadLine());

            }
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > mayor2)
                {
                    if (array[i] > mayor2)
                    {
                        mayor2 = mayor;
                        mayor = array[i];
                    }
                    else
                    {
                        mayor2 = array[i];
                    }
                }
                
            }
            Console.WriteLine("Los números mayores son " + mayor + " y " + mayor2);
        }
        
    }
}
